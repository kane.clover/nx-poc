import './affinity-suite-feature-orders.module.css';

/* eslint-disable-next-line */
export interface AffinitySuiteFeatureOrdersProps {}

export function AffinitySuiteFeatureOrders(
  props: AffinitySuiteFeatureOrdersProps
) {
  return (
    <div>
      <h1>Welcome to AffinitySuiteFeatureOrders!</h1>
    </div>
  );
}

export default AffinitySuiteFeatureOrders;
