import { render } from '@testing-library/react';

import AffinitySuiteFeatureOrders from './affinity-suite-feature-orders';

describe('AffinitySuiteFeatureOrders', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AffinitySuiteFeatureOrders />);
    expect(baseElement).toBeTruthy();
  });
});
