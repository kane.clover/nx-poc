# affinity-suite-feature-sales

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test affinity-suite-feature-sales` to execute the unit tests via [Jest](https://jestjs.io).
