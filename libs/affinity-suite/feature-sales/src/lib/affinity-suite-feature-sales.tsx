import './affinity-suite-feature-sales.module.css';

/* eslint-disable-next-line */
export interface AffinitySuiteFeatureSalesProps {}

export function AffinitySuiteFeatureSales(
  props: AffinitySuiteFeatureSalesProps
) {
  return (
    <div>
      <h1>Welcome to AffinitySuiteFeatureSales!</h1>
    </div>
  );
}

export default AffinitySuiteFeatureSales;
