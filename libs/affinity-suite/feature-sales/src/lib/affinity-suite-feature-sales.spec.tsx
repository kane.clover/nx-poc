import { render } from '@testing-library/react';

import AffinitySuiteFeatureSales from './affinity-suite-feature-sales';

describe('AffinitySuiteFeatureSales', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AffinitySuiteFeatureSales />);
    expect(baseElement).toBeTruthy();
  });
});
