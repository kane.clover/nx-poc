import { render } from '@testing-library/react';

import SharedCpq from './shared-cpq';

describe('SharedCpq', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SharedCpq />);
    expect(baseElement).toBeTruthy();
  });
});
