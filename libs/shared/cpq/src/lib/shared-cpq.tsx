import './shared-cpq.module.css';
import { LoaderButton } from '@affinity/shared-design-system';

/* eslint-disable-next-line */
export interface SharedCpqProps {}

export function SharedCpq(props: SharedCpqProps) {
  return (
    <div>
      <h1>Welcome to SharedCpq lazy!</h1>
      <LoaderButton loading={false}>Shared DS Component</LoaderButton>
    </div>
  );
}

export default SharedCpq;
