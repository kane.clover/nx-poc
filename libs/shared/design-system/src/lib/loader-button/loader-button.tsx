import { Button, CircularProgress, ButtonProps } from '@material-ui/core';
import './loader-button.module.css';

/* eslint-disable-next-line */
export interface LoaderButtonProps extends ButtonProps {
  loading: boolean;
}

export function LoaderButton(props: LoaderButtonProps) {
  const { loading, children, disabled, ...otherProps } = props;
  return (
    <Button
      variant="contained"
      {...otherProps}
      startIcon={
        loading ? (
          <CircularProgress color="inherit" size="1rem" />
        ) : (
          otherProps.startIcon
        )
      }
      disabled={loading || disabled}
    >
      {props.children}
    </Button>
  );
}

export default LoaderButton;
