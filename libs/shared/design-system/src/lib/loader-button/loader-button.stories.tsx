import { Story, Meta } from '@storybook/react';
import { LoaderButton, LoaderButtonProps } from './loader-button';

export default {
  component: LoaderButton,
  title: 'LoaderButton',
} as Meta;

const Template: Story<LoaderButtonProps> = (args) => (
  <LoaderButton {...args}>Things</LoaderButton>
);

export const Primary = Template.bind({});
Primary.args = {
  loading: false,
};
