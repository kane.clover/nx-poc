import { render } from '@testing-library/react';

import ExpandingCard from './expanding-card';

describe('ExpandingCard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ExpandingCard />);
    expect(baseElement).toBeTruthy();
  });
});
