import './expanding-card.module.css';

/* eslint-disable-next-line */
export interface ExpandingCardProps {}

export function ExpandingCard(props: ExpandingCardProps) {
  return (
    <div>
      <h1>Welcome to ExpandingCard!</h1>
    </div>
  );
}

export default ExpandingCard;
