import { Story, Meta } from '@storybook/react';
import { ExpandingCard, ExpandingCardProps } from './expanding-card';

export default {
  component: ExpandingCard,
  title: 'ExpandingCard',
} as Meta;

const Template: Story<ExpandingCardProps> = (args) => (
  <ExpandingCard {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
