import styles from './app.module.css';
import { lazy, Suspense } from 'react';
import { ReactComponent as Logo } from './logo.svg';
import star from './star.svg';
import { LoaderButton } from '@affinity/shared-design-system';
import { Link, Route } from 'react-router-dom';
import { Home } from './pages/home/home';
import { Settings } from './pages/settings/settings';
import { AffinitySuiteFeatureOrders } from '@affinity/affinity-suite/feature-orders';
import { AffinitySuiteFeatureSales } from '@affinity/affinity-suite/feature-sales';

const SharedCpq = lazy(() => import('./pages/SharedCpq'));

export function App() {
  return (
    <div className={styles.app}>
      <header className="flex">
        <Logo width="75" height="75" />
        <h1>Welcome to affinity-suite!</h1>
      </header>
      <main>
        <LoaderButton loading={true}>Stuff</LoaderButton>
        <LoaderButton loading={false}>Stuff</LoaderButton>
        <ul>
          <li>
            <Link to="/">Home route</Link>
          </li>
          <li>
            <Link to="/settings">Settings route</Link>
          </li>
          <li>
            <Link to="/shared-cpq">Shared CPQ route lazy</Link>
          </li>
        </ul>
        <Suspense fallback={<div>Loading...</div>}>
          <Route path="/" exact component={Home} />
          <Route path="/settings" exact component={Settings} />
          <Route path="/shared-cpq" exact component={SharedCpq} />
          <Route path="/sdfsdf" exact component={AffinitySuiteFeatureOrders} />
          <Route path="/sdfsdf" exact component={AffinitySuiteFeatureSales} />
        </Suspense>
      </main>
    </div>
  );
}

export default App;
