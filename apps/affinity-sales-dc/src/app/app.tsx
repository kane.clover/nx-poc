import styles from './app.module.css';

import { ReactComponent as Logo } from './logo.svg';
import { SharedCpq } from '@affinity/shared/cpq';

export function App() {
  return (
    <div className={styles.app}>
      <header className="flex">
        <Logo width="75" height="75" />
        <h1>Welcome to affinity-sales-dc!</h1>
      </header>
      <main>
        <SharedCpq />
      </main>
    </div>
  );
}

export default App;
