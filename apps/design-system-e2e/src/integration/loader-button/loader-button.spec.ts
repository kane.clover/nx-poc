describe('design-system: LoaderButton component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=loaderbutton--primary&args=label;'));
    
    it('should render the component', () => {
      cy.get('h1').should('contain', 'Welcome to LoaderButton!');
    });
});
